def guessing_game
  target = rand(1..100)
  puts "Guess a number between 1 and 100."
  guesses = 1
  while true
    input = gets.chomp.to_i
    if input > target
      puts "#{input} is too high!"
    elsif input < target
      puts "#{input} is too low!"
    else
      break
    end
    guesses += 1
  end
  puts "You used #{guesses} guesses. The correct number was: #{target}."
end


if __FILE__ == $PROGRAM_NAME
  file = ""
  until File.exist?(file)
    puts "Please provide a valid filename."
    file = gets.chomp
  end
  contents = File.readlines(file)
  shuffled = contents.shuffle
  File.open("#{file.split(".")[0]}-shuffled.txt", "w") do |f|
    shuffled.each do |line|
      f.puts line
    end
  end
  "Data was successfully shuffled in the #{file.split(".")[0]}-shuffled.txt"
end
